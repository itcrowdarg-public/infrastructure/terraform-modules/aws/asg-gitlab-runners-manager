resource "aws_security_group" "runner_manager_sg" {
  name        = "runner_manager_sg"
  description = "Allow ssh inbound traffic"

  ingress {
    description      = "ssh from everywhere"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "runner manager sg"
  }
}
