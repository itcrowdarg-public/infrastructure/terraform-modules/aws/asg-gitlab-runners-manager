# ASG - Gitla Runners Manager

## Usage

```terraform
module "runner-manager" {
  source = "git::https://gitlab.com/itcrowdarg-public/infrastructure/terraform-modules/aws/asg-gitlab-runners-manager.git?ref=X.Y"

  runner_manager_instance_type = "t3.nano"
  runner_instance_type         = "m4.xlarge"
  config_toml_concurrent       = "5"
  config_toml_limit            = "20"
  config_toml_idle_count       = "0"
  config_toml_idle_time        = "3600"
  config_toml_max_builds       = "100"

  config_toml_s3_bucket_name       = var.s3_bucket_name_cache_runners
  region                           = var.region
  aws_access_key                   = var.aws_access_key
  aws_secret_key                   = var.aws_secret_key
  gitlab_users_repo_url            = var.gitlab_users_repo_url
  project_name                     = var.project_name
  gitlab_runner_registration_token = var.gitlab_runner_registration_token
}
```