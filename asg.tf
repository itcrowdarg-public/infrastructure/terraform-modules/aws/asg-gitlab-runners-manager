resource "aws_autoscaling_group" "runner_manager" {

  name_prefix = "runner_manager-"

  availability_zones   = ["${var.region}a", "${var.region}b"]

  launch_template {
    id      = aws_launch_template.runner_manager.id
    version = "$Latest"
  }

  desired_capacity = 1
  max_size         = 1
  min_size         = 1

  protect_from_scale_in = false

  tag {
    key                 = "Name"
    value               = "Runner Manager"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }

  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 0
    }
  }
}
