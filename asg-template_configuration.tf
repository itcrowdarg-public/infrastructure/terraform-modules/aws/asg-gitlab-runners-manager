resource "aws_launch_template" "runner_manager" {
  name_prefix   = "runner_manager-"
  instance_type = var.runner_manager_instance_type
  image_id      = data.aws_ami.ubuntu.id
  user_data     = base64encode(data.template_file.instance_startup_script.rendered)

  network_interfaces {
    associate_public_ip_address = true
    security_groups             = [aws_security_group.runner_manager_sg.id]
  }

  lifecycle {
    ignore_changes = [image_id]
  }

}
