resource "aws_security_group" "runner_instance_sg" {
  name        = "runner_instance_sg"
  description = "Allow inbound traffic from manager"

  ingress {
    description     = "ssh from manager"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.runner_manager_sg.id]
  }

  ingress {
    description      = "runner things from manager"
    from_port        = 2376
    to_port          = 2376
    protocol         = "tcp"
    security_groups = [aws_security_group.runner_manager_sg.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "runner instance sg"
  }
}
