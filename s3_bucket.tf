resource "aws_s3_bucket" "runner_cache_bucket" {
  bucket = var.config_toml_s3_bucket_name

  force_destroy = true

  tags = {
    Name = "Cache store for Gitlab runners"
  }
}
