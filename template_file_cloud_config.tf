data "template_file" "instance_startup_script" {
  template = file("${path.module}/user-data/cloud-init.yml.tpl")

  vars = {
    instance_startup_script = templatefile("${path.module}/user-data/instance_startup_script.sh.tpl", {
      gitlab_users_repo_url            = var.gitlab_users_repo_url
      gitlab_runner_registration_token = var.gitlab_runner_registration_token
      project_name                     = var.project_name
    })

    config_runner_token = templatefile("${path.module}/user-data/config_runner_token.sh.tpl", {})

    config_toml = templatefile("${path.module}/user-data/config.toml.tpl", {
      aws_access_key = var.aws_access_key
      aws_secret_key = var.aws_secret_key

      aws_region                       = var.region
      config_toml_runner_instance_type = var.runner_instance_type
      config_toml_concurrent           = var.config_toml_concurrent
      config_toml_limit                = var.config_toml_limit
      config_toml_idle_count           = var.config_toml_idle_count
      config_toml_idle_time            = var.config_toml_idle_time
      config_toml_max_builds           = var.config_toml_max_builds
      config_toml_s3_bucket_name       = aws_s3_bucket.runner_cache_bucket.bucket

      vpc_id    = aws_default_vpc.default.id
      subnet_id = aws_default_subnet.default_subnet.id

      security_group = aws_security_group.runner_instance_sg.name
    })

  }
}
