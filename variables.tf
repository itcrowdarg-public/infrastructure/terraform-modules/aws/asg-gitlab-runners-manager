variable "region" {
  description = "AWS region"
  type        = string
}

variable "aws_access_key" {
  description = "It should be set to the AWS_ACCESS_KEY_ID environment variable on Gitlab"
  type        = string
}
variable "aws_secret_key" {
  description = "It should be set to the AWS_SECRET_ACCESS_KEY environment variable on Gitlab"
  type        = string
}

variable "runner_manager_instance_type" {
  description = "Instance type for Runner Manager host"
  type        = string
}

variable "runner_instance_type" {
  description = "Instance type for  auto spawned Runner hosts"
  type        = string
}

variable "gitlab_users_repo_url" {
  description = "Gitlab repo with allowed users"
  type        = string
}

variable "gitlab_runner_registration_token" {
  description = "Gitlab runner registration token, comma separated"
  type        = string
}

variable "project_name" {
  description = "Project or client's name to identify runner"
  type        = string
}

variable "config_toml_concurrent" {
  type = string
}

variable "config_toml_limit" {
  type = string
}

variable "config_toml_idle_count" {
  type = string
}

variable "config_toml_idle_time" {
  type = string
}

variable "config_toml_max_builds" {
  type = string
}

variable "config_toml_s3_bucket_name" {
  type = string
}
