output "security_group_gitlab_runner_id" {
  value = aws_security_group.runner_instance_sg.id
}