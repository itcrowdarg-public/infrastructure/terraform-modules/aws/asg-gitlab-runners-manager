#!/bin/bash

###################### Install dependencies ###############
sudo apt update -y
sudo apt upgrade -y

sudo apt install -y docker.io
sudo groupadd docker
sudo usermod -aG docker ubuntu

###################### Configure SSH ######################

# create .ssh directory
mkdir -p /home/ubuntu/.ssh
chown ubuntu:ubuntu -R /home/ubuntu/.ssh

# download ssh keys from gitlab users
touch /opt/update_authorized_keys

echo '#!/bin/bash' >> /opt/update_authorized_keys
echo 'date' >> /opt/update_authorized_keys
echo '' >> /opt/update_authorized_keys
echo 'docker pull itcrowdarg/bastion-access-manager:latest' >> /opt/update_authorized_keys
echo 'docker run --rm -v /opt:/opt -e PROJECT_NAME=${project_name} -e GIT_URL=${gitlab_users_repo_url} itcrowdarg/bastion-access-manager:latest' >> /opt/update_authorized_keys
echo 'if [ -f /opt/authorized_keys ]; then' >> /opt/update_authorized_keys
echo '  chmod 600 /opt/authorized_keys' >> /opt/update_authorized_keys
echo '  chown ubuntu:ubuntu /opt/authorized_keys' >> /opt/update_authorized_keys
echo '  mv /opt/authorized_keys /home/ubuntu/.ssh' >> /opt/update_authorized_keys
echo 'else' >> /opt/update_authorized_keys
echo '  echo "No authorized keys file found"' >> /opt/update_authorized_keys
echo 'fi' >> /opt/update_authorized_keys

chmod +x /opt/update_authorized_keys

/opt/update_authorized_keys &> /var/log/update_authorized_keys.log

# Cron job
echo '0 * * * * root /opt/update_authorized_keys > /var/log/update_authorized_keys.log  2>&1' >> /etc/crontab

###################### Gitlab Runner ######################

# Install Gitlab Runner Helper Images
curl -LJO "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/deb/gitlab-runner-helper-images.deb"
sudo dpkg -i gitlab-runner-helper-images.deb

# Install Gitlab Runner
# https://docs.gitlab.com/runner/install/linux-manually.html#download
curl -LJO "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/deb/gitlab-runner_amd64.deb"
sudo dpkg -i gitlab-runner_amd64.deb

# Install Docker Machine
wget https://gitlab-docker-machine-downloads.s3.amazonaws.com/v0.16.2-gitlab.28/docker-machine-Linux-x86_64
sudo mv docker-machine-Linux-x86_64 /usr/local/bin/docker-machine
sudo chmod +x /usr/local/bin/docker-machine


# Register Gitlab Runner for all projects related to tokens.

RUNNER_IP=$(curl http://checkip.amazonaws.com)

GITLAB_RUNNER_REGISTRATION_TOKEN="${gitlab_runner_registration_token}"
for token in $${GITLAB_RUNNER_REGISTRATION_TOKEN//,/ }
do
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --token "$token" \
  --executor "docker+machine" \
  --docker-image alpine:latest \
  --description "Runner Manager for ${project_name} - AWS Public IP: $RUNNER_IP"
done

sudo gitlab-runner verify
sudo gitlab-runner start

sudo /opt/config_runner_token.sh
sudo mv /opt/config.toml /etc/gitlab-runner/config.toml

sudo gitlab-runner verify
sudo gitlab-runner stop
sudo gitlab-runner start