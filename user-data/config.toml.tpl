concurrent = ${config_toml_concurrent}
check_interval = 0

[[runners]]
REPLACE_NAME
REPLACE_URL
REPLACE_TOKEN
  executor = "docker+machine"
  limit = ${config_toml_limit}
  [runners.docker]
    image = "alpine"
    privileged = true
    disable_cache = true
  [runners.cache]
    Type = "s3"
    Shared = true
    [runners.cache.s3]
      ServerAddress = "s3.amazonaws.com"
      AccessKey = "${aws_access_key}"
      SecretKey = "${aws_secret_key}"
      BucketName = "${config_toml_s3_bucket_name}"
      BucketLocation = "${aws_region}"
  [runners.machine]
    IdleCount = ${config_toml_idle_count}
    IdleTime = ${config_toml_idle_time}
    MaxBuilds = ${config_toml_max_builds}
    MachineDriver = "amazonec2"
    MachineName = "gitlab-docker-machine-%s"
    MachineOptions = [
      "amazonec2-access-key=${aws_access_key}",
      "amazonec2-secret-key=${aws_secret_key}",
      "amazonec2-region=${aws_region}",
      "amazonec2-vpc-id=${vpc_id}",
      "amazonec2-subnet-id=${subnet_id}",
      "amazonec2-use-private-address=true",
      "amazonec2-tags=runner-manager-name,gitlab-aws-autoscaler,gitlab,true,gitlab-runner-autoscale,true",
      "amazonec2-security-group=${security_group}",
      "amazonec2-instance-type=${config_toml_runner_instance_type}",
      "engine-install-url=https://releases.rancher.com/install-docker/19.03.15.sh",
    ]
