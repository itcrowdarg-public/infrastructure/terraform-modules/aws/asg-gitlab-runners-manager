#/bin/bash

NAME=$(sudo grep "name = " /etc/gitlab-runner/config.toml)
URL=$(sudo grep "url = " /etc/gitlab-runner/config.toml)
TOKEN=$(sudo grep "token = " /etc/gitlab-runner/config.toml)

sed -i "s/REPLACE_NAME/$NAME/g" /opt/config.toml
sed -i "s@REPLACE_URL@$URL@g" /opt/config.toml
sed -i "s/REPLACE_TOKEN/$TOKEN/g" /opt/config.toml
