#cloud-config
package_update: true
package_upgrade: true

write_files:
  - content: |
      ${base64encode("${instance_startup_script}")}
    encoding: b64
    owner: root:root
    path: /opt/instance_startup_script.sh
    permissions: '0750'
  - content: |
      ${base64encode("${config_runner_token}")}
    encoding: b64
    owner: root:root
    path: /opt/config_runner_token.sh
    permissions: '0750'
  - content: |
      ${base64encode("${config_toml}")}
    encoding: b64
    owner: root:root
    path: /opt/config.toml
    permissions: '0644'

runcmd:
 - /opt/instance_startup_script.sh